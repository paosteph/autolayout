//
//  ViewController.swift
//  AutoLayout
//
//  Created by PAOLA GUAMANI on 26/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // USO DE BASE DE DATOS
    
    // para conectar un nuevo view controller al tab
    // tecla control y sobre el view y clic relation ship view controller
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // recupero el dato almacenado
        // as? le hace opcional
        // para borrar solo borrando app o seteando en ""
        let message:String? = UserDefaults.standard.value(forKey: "message") as? String
        messageTextField.text = message
        
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        titleLabel.text = messageTextField.text
        // UserDefaults es clase que obtiene Singleton Standard para guardar
        // util para string, (primitivos): booleanos, enteros, doubles, arreglos de primitivos
        // para objetos ya NO
        // forkey permite recuperar el valor que se almacena
        
        UserDefaults.standard.set(messageTextField.text, forKey: "message")
        
        // plist tambien guarda datos en formato xml
        // CORE DATA es un framework de apple para manejo de datos
        // en su ultima capa tiene la base de datos
        // capas 1: applicacion - view controller, referencia a swift
        // capa 2: nsManaged object - registro en la BD, cada fila que recuperamos es de este tipo
        // capa 3: nsManagedObject Context - guarda el estado de los objetos hasta un commit/save
        // capa 4: nsManagedObjectModel - permite modelar obj, crear entidades, nsPSCoodinator: coordina diferentes stores maneja
        // capa 5: nsPersistentStore - (transf lode la base a codigo)
        // resto capas ayudan a acceder a capa final
        // ultima capa: esta la base de datos
        
        // REALM
    }

}

