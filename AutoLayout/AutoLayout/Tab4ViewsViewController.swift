//
//  Tab4ViewsViewController.swift
//  AutoLayout
//
//  Created by PAOLA GUAMANI on 4/7/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class Tab4ViewsViewController: UIViewController {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func TapGestureView1Activaded(_ sender: Any) {
        view1.backgroundColor = .blue
    }
    
    @IBAction func TapGestureView2Activaded(_ sender: Any) {
        view2.backgroundColor = .yellow
    }
    
    @IBAction func TapGestureView3Activated(_ sender: Any) {
        view3.backgroundColor = .red
    }
    @IBAction func TapGestureView4Activated(_ sender: Any) {
        view4.backgroundColor = .black
    }
}
