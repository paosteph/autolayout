//
//  PinchViewController.swift
//  AutoLayout
//
//  Created by PAOLA GUAMANI on 27/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    @IBOutlet weak var widthC: NSLayoutConstraint!
    @IBOutlet weak var heightC: NSLayoutConstraint!
    @IBOutlet weak var squareView: UIView!
    
    //CGFloat se usa para coordenadas de pantalla
    var originalHeight: CGFloat = 0
    var originalWidth: CGFloat = 0
    var originalPosition: CGFloat = 0
    var positionRotated : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalHeight = heightC.constant
        originalWidth = widthC.constant
    }

    @IBAction func longPressActivated(_ sender: Any) {
        squareView.backgroundColor = .blue
        
    }
    
    @IBAction func pinchGestureActivated(_ sender: Any) {
        //pinch tiene escala y velocidad
        let pinch = sender as! UIPinchGestureRecognizer
        
        heightC.constant = originalHeight * pinch.scale
        widthC.constant = originalWidth * pinch.scale
        
        //print("scale: \(heightC.constant)")
        //print("velocity: \(pinch.velocity)")
        
        if(pinch.state == .ended){
            //hace que se ejecute en intervalo de 3 seg
            UIView.animate(withDuration: 1, delay: 0.5, animations: {
                self.heightC.constant = self.originalHeight
                self.widthC.constant = self.originalWidth
            })
            
            
        }
    }
    
    @IBAction func rotateGestureActivated(_ rotationGesture: UIRotationGestureRecognizer) {
        /*var positionRotated : CGFloat = 0
        if rotationGesture.state == .began{
            originalPosition = rotationGesture.rotation
            
        }else if rotationGesture.state == .changed{
            positionRotated = rotationGesture.rotation + originalPosition
            rotationGesture.view?.transform = CGAffineTransform(rotationAngle: positionRotated )
            
        }else if rotationGesture.state == .ended{
            rotationGesture.view?.transform = CGAffineTransform(rotationAngle: -positionRotated )
            //rotationGesture.view?.transform = rotationGesture.view!.transform.rotated(by: 0)
            
        }*/
        
        guard rotationGesture.view != nil else { return }
        
        if rotationGesture.state == .began || rotationGesture.state == .changed {
            rotationGesture.view?.transform = rotationGesture.view!.transform.rotated(by: rotationGesture.rotation)
            positionRotated += rotationGesture.rotation
            //print(rotationGesture.rotation)
            print(positionRotated)
            print(rotationGesture.state)
            rotationGesture.rotation = 0
        }
        print(rotationGesture.state)
        if rotationGesture.state == .ended {
            print("hola")
            rotationGesture.view?.transform = rotationGesture.view!.transform.rotated(by: -positionRotated)
            positionRotated=0
        }
        
        
    }
        
    }
    
    
    //usar gesture rotation para
    //vista .transform, angulos en radianes, gestor rotation da en rad
    //y cuando sueltes que regrese a la posicion original


