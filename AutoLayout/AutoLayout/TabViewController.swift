//
//  TabViewController.swift
//  AutoLayout
//
//  Created by PAOLA GUAMANI on 27/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //conectar como action que se dispara cuando toquemos 5 veces
    //conectar como outlet configurar en la clase otros
    @IBAction func tapGestureActivated(_ sender: Any) {
        mainView.backgroundColor = .black
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
